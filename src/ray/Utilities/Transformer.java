/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ray.Utilities;

import java.util.Set;

/**
 *
 * Handles the formatting of the final result and printing of the complexity
 * 
 * @author Steel
 */
public class Transformer {
    
    public int complexity = 0;
    
    /**
     * formats the literal to suit the sums of products model
     * 
     * @param q the combined string in the form: 00__, _101, etc 
     * @param var the variables of the boolean function  
     * @return the formatted literal
     */
    public String getProductOfSumsExp(String q, String var){
        String term = "";
        
        String temp = "";
        for(int i = 0;i < q.length();i++){
            if(q.charAt(i) == '0'){
               temp += " + " + String.valueOf(var.charAt(i));
            }
            else if(q.charAt(i) == '1'){
                temp +=" + " + String.valueOf(var.charAt(i)) + "'";
            }
        }
        for(int i = 2;i < temp.length();i++){
                term = term.concat(String.valueOf(temp.charAt(i)));
        }
        return "(" + term + ")";
    }
    
    /**
     * formats the literal to suit the product of sums model
     * 
     * @param q the combined string in the form: 00__, _101, etc 
     * @param var the variables of the boolean function
     * @return the formatted literal
     */
    public String geSumsOfProductsExp(String q, String var){
        String term = "";
        
        for(int i = 0;i < q.length();i++){
            if(q.charAt(i) == '0'){
               term += var.charAt(i) + "'";
            }
            else if(q.charAt(i) == '1'){
                term += String.valueOf(var.charAt(i));
            }
        }
        return term;
    }
    
    /**
     * 
     * formats the essential prime implicants and changes them to a minimized form  of the function
     * 
     * @param list a list of the essential prime implicants
     * @param vars the variables of the boolean function
     * @param isSumOfMinTerms SumOfMinTerms or producttsOfMaxTerms: true is SumOfMinTerms
     * @return the minimized form of the boolean function
     */
    public String changeToMinimisedFunction(Set<String> list, String vars, boolean isSumOfMinTerms){
        String result = "";
        
        vars = vars.replace(",", "");
        
        if(isSumOfMinTerms){
            String res = "";
            for (String list1 : list) {
                String temp = geSumsOfProductsExp(list1, vars);
                res = res + temp + " + ";
            }
            for(int i = 0;i < (res.length() - 2);i++){
                result = result.concat(String.valueOf(res.charAt(i)));
            }
        }else{
            for (String list1 : list) {
                String temp = getProductOfSumsExp(list1, vars);
                result =  result + temp;
            }
        }
        
        if(result.equalsIgnoreCase(" ") || result.equalsIgnoreCase("()")){
            complexity = 0;
            return "1";
        }else{
            int d = result.length();
            for(int i = 0;i < d;i++){
                if(vars.contains(String.valueOf(result.charAt(i)))){//checkk if the character is part of the entered variables
                    complexity++;
                }
            }
        }
        
        return result;
    }
    
}
