package ray.Utilities;

import java.util.ArrayList;
import ray.model.Term;

/**
 *
 * Handles the grouping of the terms based on the number of 1's or 0's in them
 * 
 * @author Steel
 */
public class Grouper {
    
   /**
     * counts the number of 0's in a binary number
     * 
     * @param bNum the number
     * @return the number of 1's in the number
     */
    public int noOfZeros(String bNum){
        int num = 0;
        
        for(int i = 0;i < bNum.length();i++){
            if(bNum.charAt(i) == '0'){
                num++;
            }
        }
        return num;
    }
    
    /**
     * counts the number of 1's in a binary number
     * 
     * @param bNum the number
     * @return the number of 1's in the number
     */
    public int noOfOnes(String bNum){
        int num = 0;
        
        for(int i = 0;i < bNum.length();i++){
            if(bNum.charAt(i) == '1'){
                num++;
            }
        }
        return num;
    }
    
    /**
     * 
     * Groups the terms by the number of ones in them, It creates a separate list for each group
     * 
     * @param globalList the list of all the Terms
     * @param varNum the number of variables of the boolean function 
     * @param isSOP tells if the numbers supplied are min-terms or max-terms, default is min-terms 
     * @return a list of lists each arranged in the order of increasing 1's.
     *
     */
    public ArrayList<ArrayList<Term>> group(ArrayList<Term> globalList, int varNum, boolean isSOP){
        ArrayList<ArrayList<Term>> groupedList = new ArrayList<>();
        ArrayList<ArrayList<Term>> tempList = new ArrayList<>();
        
        for(int i = 0;i <= varNum;i++){
            tempList.add(new ArrayList<Term>());           //create groups (from 0 to varNum)
        }
        
        try{
            if(isSOP){
                for (Term globalList1 : globalList) {
                    int num = noOfOnes(globalList1.getBinaryValue());
                    tempList.get(num).add(globalList1);
                }
            }else{
                for (Term globalList1 : globalList) {
                    int num = noOfZeros(globalList1.getBinaryValue());
                    tempList.get(num).add(globalList1);
                }
            }
        }catch(IndexOutOfBoundsException e){}
        
        for (ArrayList<Term> tempList1 : tempList) {
            if (!tempList1.isEmpty()) {
                groupedList.add(tempList1);
            }
        }
        return groupedList;
    }    
}
