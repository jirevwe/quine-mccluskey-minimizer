package ray.Utilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import ray.Forms.InterfaceForm;
import ray.model.MintermOrMaxterm;
import ray.model.Term;
/**
 * Helper class that aids grouping and combining during the minimization of the boolean function
 * 
 * @author Steel
 */
public class MintermCombiner {
    
    private final ArrayList<Term> all;  //a list of all the binary numbers, combined aand uncombined
    private final ArrayList<Term> used; //a list of all the binary numbers that were used in combination
    private boolean checkTable[][];     //a prime implicant table to check for essential prime implicants
    private int NUMOFTERMS = 0;

    public MintermCombiner() {
        this.all = new ArrayList<>();
        this.used = new ArrayList<>();
    }
   
    /**
     * @param primes a list of the prime implicants
     * @param mintermsOrMaxterms a list of the min-terms or the max-terms
     * @return a list of the prime implicants
     */
    public Set<String> selectEssentialPrimeImplicants(ArrayList<Term> primes, ArrayList<MintermOrMaxterm> mintermsOrMaxterms){
            checkTable = new boolean[mintermsOrMaxterms.size()][primes.size()];

            //essential prime implicant table
            for (int j = 0;j < primes.size();j++) {
                for(int i = 0;i < mintermsOrMaxterms.size();i++){
                    checkTable[i][j] = primes.get(j).needMinterm(mintermsOrMaxterms.get(i).getValue()); //stores true if the minterm is in the 
                                                                                                        //minterm's decimal value
                }                                                                        
            }   

            Set<String> essentialPrimes = new HashSet<>(); // essential primes: to be returned
            boolean[] circledMinterms = new boolean[mintermsOrMaxterms.size()]; // what minterms are "circled" or "used"
            
            // initialize circlesMinterms to all false
            for (int i= 0; i < mintermsOrMaxterms.size(); i++) {
                circledMinterms[i] = false;
            }
            
            // Checking columns and taking rows (implicants):
            NUMOFTERMS = mintermsOrMaxterms.size();    //the number of terms (min-terms or max-terms) 
            while ( !checkIfAllTermsUsed( circledMinterms ) ) {
                // arrange columns in order of how many checks they have in ascending order
                // find the column with the least number of checks:
                int minimumCheckCol = 0;
                int minimumCheckVal = primes.size() + 1;
                for (int col = 0; col < mintermsOrMaxterms.size(); col++) {
                    int numChecksInCol = 0;
                    if ( !circledMinterms[col] ) { // don’t check any minterms that are circled
                        for (int row = 0; row < primes.size(); row++) {
                            if (checkTable[col][row]) {
                                //there is a check. Add 1 to the number of checks in the column
                                 numChecksInCol++;
                            }
                        }
                        if ( numChecksInCol < minimumCheckVal ) {
                            // There are fewer checks in this column than the current choice.
                            minimumCheckVal = numChecksInCol;
                            minimumCheckCol = col;
                        }
                    }
                }
                
                // find row with most additional checks from column minimumCheckCol
                int maximumCheckRow = 0;
                int maximumCheckVal = 0;
                for (int row = 0; row < primes.size(); row++) {
                    if ( checkTable[minimumCheckCol][row] ) {
                    // There is a check in this row that is in minimumCheckCol. Count the number of additional checks in this row.
                        int checksInRow = 0;
                        for (int col = 0; col < mintermsOrMaxterms.size(); col++) {
                            if ( checkTable[col][row] && !circledMinterms[col] ) {
                                // There is a check, and its minterm is not circled.
                                checksInRow++;     
                            }
                        }
                    if ( checksInRow > maximumCheckVal ) {
                        // There are more additional checks in this row than the current choice.
                        maximumCheckVal = checksInRow;
                        maximumCheckRow = row;
                    }
                }
                }
                // Circle all minterms with checks in maxChecksRow:
                for ( int col = 0; col < mintermsOrMaxterms.size(); col++ ) {
                    if ( checkTable[col][maximumCheckRow] ) {
                        circledMinterms[col] = true;
                    }
                }
                // The implicant at maximumCheckRow is essential. Add it:
                for(int i  = 0;i < InterfaceForm.NUMOFVARIABLES;i++){
                    essentialPrimes.add(primes.get(maximumCheckRow).getBinaryValue());
                }
            }
            return essentialPrimes; //Return the essentialPrimes list which contains the essential prime implicants.
        
    }

    /**
    * checks to see if all the min-terms in "selectEssentialPrimeImplicants" have been used.
    * @param mintermsDoneArray
    */
    private boolean checkIfAllTermsUsed( boolean[] mintermsDoneArray ) {
        for ( int ix = 0; ix < NUMOFTERMS; ix++) {
            if ( mintermsDoneArray[ix] == false ) {
                return false;
            }
        }
        return true;
    }

    /**
     * gets the prime implicants for a boolean function
     * 
     * @param list A list of the Terms grouped by the number of 1's or 0's they contain
     * @return a list of the prime implicants
     */
    public ArrayList<Term> selectPrime(ArrayList<ArrayList<Term>> list){
        if(list.size() > 1){  //if the groups are more than one combine them

            recombine(list);
            ArrayList<Term> primeImp = new ArrayList<>();
            ArrayList<String> chkr = new ArrayList<>();

            //add the numbers not in the USED list to the PRIME IMPLICANT list from the ALL list
            for(Term x : all){
                if(!used.contains(x) && !chkr.contains(x.getBinaryValue())){
                    primeImp.add(x);
                    chkr.add(x.getBinaryValue());
                }
            }
            return primeImp;
        }else{ //if there is just one group (checker board kind of k-map), they are all prime implicants 
            ArrayList<Term> primeImp = new ArrayList<>();
            try{
                //since its just one group, add all the items to the prime implicant group list
                for(Term x : list.get(0)){
                    primeImp.add(x);
                }
            }catch(IndexOutOfBoundsException e){}
            
            return primeImp;
        }
    }
    
    /**
     * combines the groups in a list, to form a new list of the combined Terms
     * The function is recursive in nature, it keeps going as long as c (The COUNTER) isn't 
     * 
     * @param list A list of the binary numbers grouped by the number of 1's or 0's they contain
     * @return a list of the prime implicants 
     */
    public ArrayList<ArrayList<Term>> recombine(ArrayList<ArrayList<Term>> list){
        ArrayList<Term> tt;
        ArrayList<ArrayList<Term>> temp = new ArrayList<>();
        
        for (ArrayList<Term> list1 : list) {
            for (Term list11 : list1) {
                if (!all.contains(list11)) {
                    all.add(list11);
                }
            }
        }
        
        //if list is empty return list
        if(!list.isEmpty()){
            int f = list.size() - 1;
            for(int z = 0;z < f;z++){
                tt = combineGroup(list.get(z), list.get(z + 1));  
                temp.add(tt);
            }
            
            return recombine(temp);
        }
        return list;
    }
    
    /**
     * combines two groups of numbers to form a new group of numbers
     * 
     * @param lowerList a list that contains binary numbers with the same number of 1's or 0's
     * @param higherList a list that contains binary numbers with the same number of 1's or 0's,
     *              one more than the binary numbers in lowerList
     * @return an list of the newly combined numbers 
     */
    public ArrayList<Term> combineGroup(ArrayList<Term> lowerList, ArrayList<Term> higherList){
        ArrayList<Term> temp = new ArrayList<>();
        ArrayList<Term> tmp = new ArrayList<>();
        
        for (Term lowerList1 : lowerList) {
            for (Term higherList1 : higherList) {
                String bVal = combineStr(lowerList1.getBinaryValue(), higherList1.getBinaryValue());
                String dVal = lowerList1.getDecimalValue() + "," + higherList1.getDecimalValue();
                if (bVal != null) {
                    
                    tmp.add(new Term(bVal, dVal));
                    
                    if (!used.contains(lowerList1)) {
                        used.add(lowerList1);
                    }
                    if (!used.contains(higherList1)) {
                        used.add(higherList1);
                    }
                }
            }
        }
        for (Term tmp1 : tmp) {
            if (!tmp.isEmpty() && !temp.contains(tmp1)) {
                temp.add(tmp1);
            }
        }
        return temp;
    }
    
    /**
     * Checks if two binary numbers can be combined
     * 
     * @param a the first number
     * @param b the second number
     * @return true (if the differ by a bit) and false (if they differ by more than one bit)
     */
    public boolean isCombinable(String a, String b) {
        if (a.length() == b.length() && !a.equalsIgnoreCase(b)) {
            int sameBits = 0;
            int f = a.length();
            for (int i = 0; i < f; i++) {
                if (a.charAt(i) == b.charAt(i)) {
                    sameBits++;
                }
            }
            if (sameBits == (a.length() - 1)) {
                return true;
            }
        }
        return false;
    }

    /**
     * combines two binary numbers if they differ by a bit, returns null if they differ by more than one bit
     * 
     * @param a the first number
     * @param b the second number
     * @return  a combined version of the string (if the differ by a bit) and null (if they differ by more than one bit)
     */
    public String combineStr(String a, String b) {
        String res = "";
        if (isCombinable(a, b)) {
            int f = a.length();
            for (int i = 0; i < f; i++) {
                if (a.charAt(i) != b.charAt(i)) {
                    res = res + "_";
                    continue;
                }
                res = res + a.charAt(i) + "";
            }
            return res;
        } else {
            return null;
        }   
    }
}