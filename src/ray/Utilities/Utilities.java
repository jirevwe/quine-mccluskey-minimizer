/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ray.Utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JTextField;
import ray.model.MintermOrMaxterm;

/**
 *  Utility class that does some random work
 * 
 * @author Steel
 */
public class Utilities {
    
    /**
     * generates a random sequence of terms
     * 
     * @param x the JTextField for the terms (min terms or max-terms)
     * @param z the JTextField for the don't cares
     * @param y the JTextField to get the number of variables from
     */
    public void generateRandom(JTextField x, JTextField z, JTextField y){
        
        int num = (int)Math.pow(2, getVaribles(y.getText()));
        int numOfTerms = (int)(System.currentTimeMillis() % num);
        Set<Integer> rand = new HashSet<>();
        List<Integer> sortedRandom; 
        //for the terms textfield
        String temp = "";
        String vars = "";
        //for the dontcare textfield
        String temp2 = "";
        String vars2 = "";
        int t;
        
        if(numOfTerms == 0){
            t = (int)System.currentTimeMillis() % num;
            rand.add(t);
        }else{
            while(rand.size() < numOfTerms){
                t = (int)System.currentTimeMillis() % num;
                rand.add(t);
            }
        }
        
        sortedRandom = new ArrayList<>(rand);
        Collections.sort(sortedRandom);
        
        int a = (int)System.currentTimeMillis() % rand.size();
            
        //create two lists from the original
        Set<String> forTerms = new HashSet<>();
        Set<String> forDontCares = new HashSet<>();
        
        for(int i = 0;i < a;i++){
            forTerms.add(sortedRandom.get(i).toString());
        }
        for(int j = a;j < sortedRandom.size();j++){
            forDontCares.add(sortedRandom.get(j).toString());
        }
                
        for (String tmp : forTerms) {
            temp = temp.concat(tmp + ",");
        }
        
        int f = temp.length() - 1;
        for(int i = 0;i < f;i++){
            vars = vars.concat(String.valueOf(temp.charAt(i)));
        }
        
         for (String tmp2 : forDontCares) {
            temp2 = temp2.concat(tmp2 + ",");
        }

        int ff = temp2.length() - 1;
        for(int i = 0;i < ff;i++){
            vars2 = vars2.concat(String.valueOf(temp2.charAt(i)));
        }
        
        z.setText(vars2);
        x.setText(vars);
    }
    
    /**
    * extracts text from a JTextfield and makes them into a list 
    * 
     * @param temp the string of numbers
     * @return a list of numbers in the JTextField
     */
    public ArrayList<MintermOrMaxterm> toMintermMaxtermArrayList(String temp){
        ArrayList<MintermOrMaxterm> x = new ArrayList<>();
        String t = "";
        int f = temp.length();
        for(int i = 0;i < f;i++){
            if(temp.charAt(i) == ',' || temp.charAt(i) == '.'){
                x.add(new MintermOrMaxterm(t));
                t = "";
                continue;
            }
            t += temp.charAt(i);
            t = t.replace(" ", "");
        }
        x.add(new MintermOrMaxterm(t));

        return x;
    }
    
    /**
     * merges the terms in the "Enter terms" and "Don't Cares" JTextFields
     * 
     * @param mintermsOrMaxterms the terms in the Enter terms JTextField
     * @param dontCares the terms in the Don't Care JTextField
     * @return the terms combined or just the terms in the Enter terms JTextField if the Don't Care JTextField is empty
     */
    public String dontCaresAndMintermsOrMaxterms(String mintermsOrMaxterms, String dontCares){
        
        if(dontCares.isEmpty()){
            return mintermsOrMaxterms;
        }
        
        return mintermsOrMaxterms + "," + dontCares;
    }
    
    /**
     * counts the number of variables of the boolean function 
     * 
     * @param vars the inputted variables
     * @return the number of variables for a boolean function
     */
    public int getVaribles(String vars){
        int c = 0;
        int f = vars.length();
        for(int x = 0;x < f;x++){
            if(!(vars.charAt(x) == ',')){
               c++; 
            }
        }
        return c;
    }
}
