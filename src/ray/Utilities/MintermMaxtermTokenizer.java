/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ray.Utilities;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Utility class that handles the splitting of the inputted terms
 * 
 * @author Steel
 */
public class MintermMaxtermTokenizer {
    
    /**
     * 
     * Gets the corresponding Min-term or Max-term equivalent of the inputted terms
     * 
     * @param list a list of Min-terms or Max-terms separated by commas 
     *             e.g:  [1,3,5,7,13,15]
     * @param max the number of the function
     * @return a string containing the numbers not found in the inputted list
     * 
     */
    public String exchange(ArrayList<String> list, int max){
        ArrayList<String> res = new ArrayList<>();
        String t = "";
        String result = "";
        int maxNum = (int)(Math.pow(2, max));
        
        for(int i = 0;i < maxNum;i++){
            if(!list.contains(String.valueOf(i))){
                res.add(String.valueOf(i));
            }
        }
        for(String x : res){
            result += "," + x;
        }
        for(int i = 1;i < result.length();i++){
            t += result.charAt(i);
        }
        return t;
    } 
    
    /**
     * 
     * Makes the inputted terms into a list
     * 
     * @param theTerms a string of the Min-terms or Max-terms
     * @param max the number of variables of the function
     * @param terms the Terms JTextField: holds the min-terms or max-terms
     * @param dontCare the Don't Care JTextField: holds the don't care terms
     * @return a list of the numbers contained in the string
     */
    public ArrayList<String> tokenize(String theTerms, int max, JTextField terms, JTextField dontCare){
        ArrayList<String> x  = new ArrayList<>();
        String temp = "";
        boolean error = false;
       
        try{
            
            for(int i = 0;i < theTerms.length();i++){
                if(theTerms.charAt(i) == ',' || theTerms.charAt(i) == '.'){
                    int theNum = Integer.parseInt(temp);
                    if(theNum >= (Math.pow(2, max))){
                        JOptionPane.showMessageDialog(null, "One of the numbers you entered: " + temp + " exceeds or is equal to  2^" 
                                + max + ": " + Math.pow(2, max) + "\n"
                                + "Please enter required input or change the number of variables");
                        terms.setText("");
                        dontCare.setText("");
                        error = true; //an error has occured
                        break;
                    }
                    x.add(temp);
                    temp = "";
                    continue;
                }
                temp += theTerms.charAt(i);
                temp = temp.replace(" ", "");
            }
            
            if(Integer.parseInt(temp) >= (Math.pow(2, max)) && !error){
                JOptionPane.showMessageDialog(null, "One of the numbers you entered: " + temp + " exceeds or is equal to  2^" 
                        + max + ": " + Math.pow(2, max) + "\n"
                        + "Please enter required input or change the number of variables");
                terms.setText("");
                dontCare.setText("");
            }
            x.add(temp);
            
        }catch(NumberFormatException e){
            JOptionPane.showMessageDialog(null, "Illegal Input Format, please enter valid input");
            terms.setText("");
            dontCare.setText("");
        }
        
        return x;
    }   
}
