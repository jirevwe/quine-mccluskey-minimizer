package ray.Utilities;
/**
 *
 * @author Steel
 */
public class BinaryConvert {
	
        /**
         * converts a number to binary
         * 
         * @param n the number
         * @return the binary equivalent of n
         */
	public String getBinaryValue(int n) {
            int c = n / 2;
            int r = n % 2;
            if(c == 0) {
                return r + "";
            }
            return getBinaryValue(c) + r;
	}
	
        /**
         * 
         * adds zeros to the front of a number based on the number of variables in the function
         * 
         * @param n a binary number
         * @param max the number of variables of the boolean function
         * @return the binary number with zeros in front of it
         */
	public String addLeadingZeros(String n, int max) {
            int t = n.length();
            String m = "";
            for(int i = 0;i < (max - t);i++) {
                m += "0";
            }
            return m + n;
	}
}
