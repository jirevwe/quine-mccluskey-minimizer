/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ray.model;

/**
 *
 * Abstract term model, each term has a binary an the decimal values that combined to form it
 *          e.g (00_1, (1,3)) 0r (0_0_, (0,2,8,10))
 * 
 * @author Steel
 */
public class Term {

    private final String binaryValue;    
    private final String decimalValue;
    private boolean used; 

    public boolean isUsed() {
        return used;
    }
    
    public void setUsed(boolean used) {
        this.used = used;
    }

    //create a new Term
    public Term(String bVal, String dVal){
        this.used = false;
        this.binaryValue = bVal;
        this.decimalValue = dVal;
    }

    public boolean needMinterm(String s){
        String[] d = this.decimalValue.split(",");
        for (String d1 : d) {
            if (s.equalsIgnoreCase(d1)) {
                return true;
            }
        }
        return false;
    }
    
    //get the binary value of a term
    public String getBinaryValue() {
        return binaryValue;
    }

    //get the decimal value of a term
    public String getDecimalValue() {
        return decimalValue;
    }
}
