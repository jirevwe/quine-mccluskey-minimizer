/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ray.model;

import java.util.ArrayList;

/**
 * Abstract Min-term/Max-term model, is used in the selection of the essential prime implicants 
 * 
 * @author Steel
 */
public class MintermOrMaxterm {
    
    private int count;
    private final String value;
    private final ArrayList<Term> term;
   
    public ArrayList<Term> getTerm() {
        return term;
    }

    public void addTerm(Term t) {
        this.incCount();
        this.term.add(t);
    }

    public MintermOrMaxterm(String val){
        this.count = 0;
        this.value = val;
        term = new ArrayList<>();
    }
    
    public int getCount() {
        return count;
    }

    public void incCount() {
        this.count++;
    }

    public String getValue() {
        return value;
    }
}
